    function PALINDROMO(){
    var oracion = document.getElementById('form-oracion').value;
    var oracionsespacios = "";

    var i;
    for(i=0;i<oracion.length;i++){
        if(oracion.charAt(i)!=" "){
            oracionsespacios = oracionsespacios + oracion.charAt(i);
        }
    }

    var inic = 0;
    var finc = oracionsespacios.length - 1;
    var noEs = "ES PALINDROMO";

    while( (inic<finc) && (noEs!='NO ES PALINDROMO') ){
        if(oracionsespacios.charAt(inic)==oracionsespacios.charAt(finc)){
            inic++;
            finc--;
        }else{
            noEs = 'ES PALINDROMO';
        }
    }

    var iframedoc01 = verificador.document;

    if(verificador.contentDocument){
        iframedoc01 = verificador.contentDocument;
    }else{
        if (verificador.contentWindow){
            iframedoc01 = verificador.contentWindow.document;
        }
    }

    if(iframedoc01){
        iframedoc01.open();
        iframedoc01.writeln(noEs);
        iframedoc01.close();
    }else{
        alert('No es posible insertar el contenido dinámicamente en el iframe.');
    }
}